package com.example.indy.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.example.indy.model.rest.PriceRestModel;
import com.example.indy.util.TestUtils;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private static PriceRestModel priceRestModel01;
    private static PriceRestModel priceRestModel02;
    private static PriceRestModel priceRestModel03;
    private static PriceRestModel priceRestModel04;

    @BeforeAll
    static void setup() {
        priceRestModel01 = new PriceRestModel("35455", 1L, "1",
                Instant.parse("2020-06-14T00:00:00Z"), Instant.parse("2020-12-31T23:59:59Z"),
                BigDecimal.valueOf(35.50));
        priceRestModel02 = new PriceRestModel("35455", 1L, "2",
                Instant.parse("2020-06-14T15:00:00Z"), Instant.parse("2020-06-14T18:30:00Z"),
                BigDecimal.valueOf(25.45));
        priceRestModel03 = new PriceRestModel("35455", 1L, "3",
                Instant.parse("2020-06-15T00:00:00Z"), Instant.parse("2020-06-15T11:00:00Z"),
                BigDecimal.valueOf(30.50));
        priceRestModel04 = new PriceRestModel("35455", 1L, "4",
                Instant.parse("2020-06-15T16:00:00Z"), Instant.parse("2020-12-31T23:59:59Z"),
                BigDecimal.valueOf(38.95));
    }

    @Test
    public void findAll_01_should_be_OK() throws Exception {
        assertGet("2020-06-14T10:00:00Z", "35455", "1", priceRestModel01);
    }

    @Test
    public void findAll_02_should_be_OK() throws Exception {
        assertGet("2020-06-14T16:00:00Z", "35455", "1", priceRestModel02);
    }

    @Test
    public void findAll_03_should_be_OK() throws Exception {
        assertGet("2020-06-14T21:00:00Z", "35455", "1", priceRestModel01);
    }

    @Test
    public void findAll_04_should_be_OK() throws Exception {
        assertGet("2020-06-15T10:00:00Z", "35455", "1", priceRestModel03);
    }

    @Test
    public void findAll_05_should_be_OK() throws Exception {
        assertGet("2020-06-16T21:00:00Z", "35455", "1", priceRestModel04);
    }

    private void assertGet(final String publicatinDate, final String productId,
            final String brandId, PriceRestModel priceRestModel) throws Exception {

        mockMvc.perform(
                        get("/api/v1/prices")
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("publication_date", publicatinDate)
                                .param("product_id", productId)
                                .param("brand_id", brandId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(TestUtils.asJsonString(priceRestModel)));

    }


}
