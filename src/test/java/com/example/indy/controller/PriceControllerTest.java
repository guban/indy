package com.example.indy.controller;

//import static org.mockito.Mockito.when;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.example.indy.exception.PriceNotFoundException;
import com.example.indy.model.db.CurrencyEnum;
import com.example.indy.model.db.Price;
import com.example.indy.model.rest.PriceRestModel;
import com.example.indy.service.PriceService;
import java.math.BigDecimal;
import java.time.Instant;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class PriceControllerTest {

    @Mock
    private PriceService priceService;

    @InjectMocks
    private PriceController priceController;

    @Autowired
    private MockMvc mockMvc;

    private Price price;
    private PriceRestModel priceRestModel;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(priceController).build();
        price = new Price(1L, Instant.parse("2020-06-14T00:00:00Z"),
                Instant.parse("2020-12-31T23:59:59Z"), "1", "35455", 0,
                BigDecimal.valueOf(35.50),
                CurrencyEnum.EUR);
        priceRestModel = new PriceRestModel("35455", 1L, "1", Instant.parse("2020-06-14T00:00:00Z"),
                Instant.parse("2020-12-31T23:59:59Z"), BigDecimal.valueOf(35.50));
    }

    @AfterEach
    public void afterEach() {
        price = null;
        priceRestModel = null;
    }

    @Test
    public void findAll_should_be_OK() throws Exception {

        when(priceService.findPriceByParams(price.getProductId(),
                String.valueOf(price.getBrandId()), Instant.parse("2020-06-14T05:00:00Z")))
                .thenReturn(priceRestModel);

        mockMvc.perform(
                        get("/api/v1/prices")
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("publication_date", "2020-06-14T05:00:00Z")
                                .param("product_id", "35455")
                                .param("brand_id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void findAll_should_be_error_404() throws Exception {

        when(priceService.findPriceByParams(price.getProductId(), "5",
                Instant.parse("2020-06-14T05:00:00Z")))
                .thenThrow(new PriceNotFoundException());

        mockMvc.perform(
                        get("/api/v1/prices")
                                .contentType(MediaType.APPLICATION_JSON)
                                .param("publication_date", "2020-06-14T05:00:00Z")
                                .param("product_id", "35455")
                                .param("brand_id", "5"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andDo(MockMvcResultHandlers.print());

    }

}
