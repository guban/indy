package com.example.indy.init;

import com.example.indy.model.db.CurrencyEnum;
import com.example.indy.model.db.Price;
import com.example.indy.repository.PriceRepository;
import java.math.BigDecimal;
import java.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDataBase {

    private static final Logger logger = LoggerFactory.getLogger(LoadDataBase.class);

    @Bean
    CommandLineRunner initDataBase(PriceRepository priceRepository) {

        return args -> {
            logger.info("Loading " + priceRepository.save(
                    new Price(1L, Instant.parse("2020-06-14T00:00:00Z"),
                            Instant.parse("2020-12-31T23:59:59Z"), "1", "35455", 0,
                            BigDecimal.valueOf(35.50), CurrencyEnum.EUR)));

            logger.info("Loading " + priceRepository.save(
                    new Price(1L, Instant.parse("2020-06-14T15:00:00Z"),
                            Instant.parse("2020-06-14T18:30:00Z"), "2", "35455", 1,
                            BigDecimal.valueOf(25.45), CurrencyEnum.EUR)));

            logger.info("Loading " + priceRepository.save(
                    new Price(1L, Instant.parse("2020-06-15T00:00:00Z"),
                            Instant.parse("2020-06-15T11:00:00Z"), "3", "35455", 1,
                            BigDecimal.valueOf(30.50), CurrencyEnum.EUR)));

            logger.info("Loading " + priceRepository.save(
                    new Price(1L, Instant.parse("2020-06-15T16:00:00Z"),
                            Instant.parse("2020-12-31T23:59:59Z"), "4", "35455", 1,
                            BigDecimal.valueOf(38.95), CurrencyEnum.EUR)));
        };

    }

}
