package com.example.indy.repository;

import com.example.indy.model.db.Price;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("select p from Price p "
            + "where p.productId = :productId "
            + "and p.brandId = :brandId "
            + "and p.startDate <= :publicationDate and p.endDate >= :publicationDate "
            + "order by p.priority desc ")
    List<Price> findAllByParams(String productId, Long brandId, Instant publicationDate);

}
