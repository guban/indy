package com.example.indy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndyApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndyApplication.class, args);
	}

}
