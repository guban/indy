package com.example.indy.service;

import com.example.indy.model.rest.PriceRestModel;
import com.example.indy.model.rest.PriceRestModelBuilder;
import com.example.indy.exception.PriceNotFoundException;
import com.example.indy.model.db.Price;
import com.example.indy.repository.PriceRepository;
import java.time.Instant;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class PriceService {

    private final PriceRepository priceRepository;

    public PriceService(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    public PriceRestModel findPriceByParams(String productId, String brandId, Instant publicationDate) {

        List<Price> prices = priceRepository.findAllByParams(productId, Long.parseLong(brandId),
                publicationDate);

        return prices.stream()
                .findFirst()
                .map(this::toRestModel)
                .orElseThrow(PriceNotFoundException::new);
    }

    /**
     * Convierte una entidad {@link Price} en un modelo {@link PriceRestModel}
     */
    public PriceRestModel toRestModel(final Price price) {
        return new PriceRestModelBuilder()
                .setProductId(price.getProductId())
                .setBrandId(price.getBrandId())
                .setPriceList(price.getPriceList())
                .setStartDate(price.getStartDate())
                .setEndDate(price.getEndDate())
                .setPrice(price.getPrice())
                .build();
    }

}
