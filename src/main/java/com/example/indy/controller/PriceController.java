package com.example.indy.controller;

import com.example.indy.model.rest.PriceRestModel;
import com.example.indy.service.PriceService;
import java.time.Instant;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PriceController {

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping("/api/v1/prices")
    PriceRestModel findAll(
            @RequestParam(name = "publication_date") Instant publicationDate,
            @RequestParam(name = "product_id") String productId,
            @RequestParam(name = "brand_id") String brandId) {

        return priceService.findPriceByParams(productId, brandId, publicationDate);

    }

}
