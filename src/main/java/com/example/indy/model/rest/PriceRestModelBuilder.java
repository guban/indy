package com.example.indy.model.rest;

import java.math.BigDecimal;
import java.time.Instant;

public class PriceRestModelBuilder {

    private String productId;
    private Long brandId;
    private String priceList;
    private Instant startDate;
    private Instant endDate;
    private BigDecimal price;

    public PriceRestModelBuilder setProductId(String productId) {
        this.productId = productId;
        return this;
    }

    public PriceRestModelBuilder setBrandId(Long brandId) {
        this.brandId = brandId;
        return this;
    }

    public PriceRestModelBuilder setPriceList(String priceList) {
        this.priceList = priceList;
        return this;
    }

    public PriceRestModelBuilder setStartDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public PriceRestModelBuilder setEndDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public PriceRestModelBuilder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public PriceRestModel build() {
        return new PriceRestModel(productId, brandId, priceList, startDate, endDate, price);
    }
}