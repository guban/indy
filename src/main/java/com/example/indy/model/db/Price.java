package com.example.indy.model.db;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "PRICES", indexes = @Index(name = "IDX_BRAND_PRODUCT", columnList = "brandId, productId"))
public class Price {

    @Id
    @GeneratedValue
    private Long id;

    private Long brandId; // fk, falta la relacion de dependencia con la tabla BRANDS
    private Instant startDate;
    private Instant endDate;
    private String priceList;
    private String productId;
    private Integer priority;
    private BigDecimal price;
    private CurrencyEnum curr;

    public Price() {
    }

    public Price(Long brandId, Instant startDate, Instant endDate, String priceList,
            String productId, Integer priority, BigDecimal price, CurrencyEnum curr) {
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.productId = productId;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getPriceList() {
        return priceList;
    }

    public void setPriceList(String priceList) {
        this.priceList = priceList;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public CurrencyEnum getCurr() {
        return curr;
    }

    public void setCurr(CurrencyEnum curr) {
        this.curr = curr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Price price1 = (Price) o;
        return id.equals(price1.id) && Objects.equals(brandId, price1.brandId)
                && Objects.equals(startDate, price1.startDate) && Objects.equals(
                endDate, price1.endDate) && Objects.equals(priceList, price1.priceList)
                && Objects.equals(productId, price1.productId) && Objects.equals(
                priority, price1.priority) && Objects.equals(price, price1.price)
                && curr == price1.curr;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brandId, startDate, endDate, priceList, productId, priority, price,
                curr);
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", brandId=" + brandId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", priceList='" + priceList + '\'' +
                ", productId='" + productId + '\'' +
                ", priority=" + priority +
                ", price=" + price +
                ", curr=" + curr +
                '}';
    }
}
